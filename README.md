# More or less a playground project to try stuff out

# Stack

- **LMDB for key-value store**
- **Express for backend**
- **Vue for frontend**
- **NodeJS for runtime**

---

# Running the app

**With docker compose**

```
docker-compose up --build -d
```

**With npm**

**Install with npm first**

- Start backend

```
npm run start-backend
```

- Start frontend

```
npm run start-frontend
```

---

# Documentation

- With typedoc

```
npm run docs
```

---

# Changelog

## [0.1.3 2022-03-24]

### Added

- The backend now hashes passwords through SHA256 before storing in database
- Passwords were already being ran through bcrypt on the client side

## [0.1.2 2022-03-24]

### Changed

- Changed frontend to typescript

## [0.1.1 2022-03-23]

### Added

- Added Docker for containerization and convenience

## [0.1.0 2022-03-22]

### Added

- Added login component that saves user credentials to database

---
