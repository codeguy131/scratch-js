import express from "express";
import cors from "cors";
import { router } from "./router";

/**
 * Main server configuration
 *
 * @public
 * @module
 */

// Port to listen on
const PORT = 8081;

// Initialize app
const app = express();
app.use(
  cors({
    origin: "*",
  })
);

// Enable parsing requests in json
app.use(express.json());

// Use router for endpoints
app.use("/", router);

app.listen(PORT, () => {
  console.log(`Running on port ${PORT}`);
});
