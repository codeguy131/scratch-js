import assert from "assert";
import { Database as database, open } from "lmdb";

// File path for LMDB
const DB_PATH = "./userdb";

/**
 * Object for database operations
 * @public
 */
export default class Database {
  db: database;

  constructor() {
    this.db = open({
      path: DB_PATH,
      //encoding: "ordered-binary",
      compression: true,
    });
  }

  /**
   * Get the user info from the database by username
   */
  async get(username: string) {
    if (!this.db.doesExist(username)) {
      console.log(`User does not exist: ${username}`);
      return false;
    }

    const entry = {
      username: username,
      password: this.db.get(username).password,
      role: this.db.get(username).role,
    };

    return entry;
  }

  /**
   * Insert user data into database
   */
  async insert(username: string, password: string, role: string) {
    try {
      assert(this.db.getEntry(username));
      return "User already exists";
    } catch {
      return await this.db
        .put(username.toString(), {
          password: password.toString(),
          role: role.toString(),
        })
        .then(() => {
          return "Successfully inserted user data";
        })
        .catch((err) => {
          return `Problem inserting into database: ${err}`;
        });
    }
  }
}
