import * as bcrypt from "bcryptjs";
import * as jwt from "jsonwebtoken";
import "dotenv/config";
import { Router } from "express";
import Database from "../db";

/**
 * **Resposible for log in functionality**
 * @public
 */
export const LogInRoute = Router();

/**
 * **Handles a post request to login endpoint**
 *
 * **Will send a json web token if successful (coming soon)**
 * @public
 */
LogInRoute.post("/login", async (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  if (!(username && password)) {
    return res.json("All inputs required");
  }

  // Get new database connection
  const conn = new Database();

  // Get user entry from database
  await conn
    .get(username)
    .then(async (resp) => {
      // Return error message
      if (!resp) {
        res.status(401).json("Bad data");
        return console.log(resp);
      }

      // Check user supplied password against saved one
      const match = await bcrypt.compare(password, resp.password);
      if (!match) {
        res.status(401).json("Bad data");
        return console.log("Bad password");
      }

      // Create token payload data
      const payload = {
        username,
        password: resp.password,
        role: resp.role,
        expiresIn: "8H",
        algorithm: "HS512",
      };

      // Check for JWT secret key in env file
      if (!process.env.JWT_SECRET) {
        return console.log("No secret key for JWT");
      }

      // Sign token with secret key
      const token = jwt.sign(payload, process.env.JWT_SECRET);
      res.json({ token });

      console.log(`Succesful login from ${username}`);

      // Close database
      await conn.db.close().catch((err) => {
        res.status(500).destroy();
        return console.log(err);
      });
    })
    .catch((err: Error) => {
      res.status(500).json(err);
      return console.log(err);
    });
});
