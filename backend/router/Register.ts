import * as bcrypt from "bcryptjs";
import { Router } from "express";
import Database from "../db";

/**
 * **Handles requests to registration endpoint**
 *
 * **Hashes password from user through SHA3-256**
 * @public
 */
export const RegRoute = Router();

RegRoute.post("/register", async (req, res) => {
  const username: string = req.body.username;
  const password: string = req.body.password;

  // Ensure all inputs exist
  if (!(username && password)) {
    console.log("Invalid request from user");
    return res.json("All input must be filled");
  }
  console.log(req.body.password);

  // Connect to database
  const conn = new Database();

  const salt = await bcrypt.genSalt(10);
  const hash: string = await bcrypt
    .hash(password, salt)
    .then((resp) => {
      return resp.toString();
    })
    .catch((err) => {
      return err;
    });

  // Insert user info into database
  await conn
    .insert(username, hash.toString(), "user")
    .then(async (resp) => {
      console.log(resp?.toString());
      return res.json(resp);
    })
    .catch((err) => {
      res.status(404).json(err);
      return console.log(err);
    });
});
