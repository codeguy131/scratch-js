import { Router } from "express";
import { LogInRoute } from "./LogIn";
import { RegRoute } from "./Register";

/**
 * **Handle routing requests
 * to different modules**
 * @public
 */
export const router = Router();

// Home endpoint
router.use("/home", (req, res) => {
  console.log(req.body);
  res.json("Hello from backend");
});

// Registration endpoint
router.post("/register", RegRoute);

//Login endpoint
router.post("/login", LogInRoute);
